function(jtsTracker,oGateConfig){

    this.oConf = oGateConfig;
	this.oTracker = jtsTracker;
    this.oConf.rDownloadFileType = new RegExp(oGateConfig.downloadFileType || /\.(7z|aac|apk|arc|arj|asf|asx|avi|azw3|bin|bz|bz2|csv|deb|dmg|doc|docx|epub|exe|flv|gif|gz|gzip|hqx|ibooks|jar|jpg|jpeg|js|mp2|mp3|mp4|mpg|mpeg|mobi|mov|movie|msi|msp|odb|odf|odg|ods|odt|ogg|ogv|pdf|phps|png|ppt|pptx|qt|qtm|ra|ram|rar|rpm|sea|sit|tar|tbz|tbz2|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xlsx|xml|z|zip)$/i);

	this.init = function(){
		this.oTracker.registerGateFunc("pageview",this.pageview,this);
	}
	
    this.pageview = function()
    {
        this.searchForLinks();
	}
    
    this.fListen =  function(oObject, sEvent, fFunction)
    {
        if (oObject.addEventListener)
        {
            oObject.addEventListener(sEvent, fFunction, false);
        }
        else
            if (oObject.attachEvent)
            {
                oObject.attachEvent("on" + sEvent, fFunction);
            }
    }

    this.searchForLinks = function ()
	{
		if (document.getElementsByTagName) 
		{
			var aLinks = document.getElementsByTagName('a');

			for (var i = 0, iMax = aLinks.length; i < iMax; i++) 
			{
                if (this.oConf.rDownloadFileType.test(aLinks[i].pathname))
				{
					this.fListen(aLinks[i], "click", this.trackDownload);
				}
			}
		}	
    }
    
	this.trackDownload = function(oEvent) 
	{
		var oElement = this.fGetElementFromEvent(oEvent);

		if (typeof oElement.href !== "undefined") {
			var sLinkVal = oElement.href;
		} else {
		    var sLinkVal = ("/" + oElement.pathname).replace(/\/\//, '');
		}


		window._jts.push({
			track: "download",
			link: sLinkVal
		});
		
    }.bind(this);
    
    // We want to support IE
    // If <a> tag has nested element, we want the parent(<a>) tag, not element itself
    this.fGetElementFromEvent = function(event)
    {
        var element = event.srcElement || event.target;

        if (element.tagName !== "A") {
            element = element.parentNode;
        }

        return element;
    }

	this.init();
}